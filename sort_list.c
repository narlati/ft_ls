/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 19:19:49 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 13:02:16 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

struct s_arg	*sort_by_type(struct s_arg *list)
{
	list = sort_invalid(list);
	list = sort_reg(list);
	return (list);
}

struct s_arg	*sort_list(struct s_arg *list, struct s_options *option)
{
	list = sort_by_type(list);
	if (option->t == 0)
		list = sort(list);
	else
		list = sort_t(list);
	if (option->r == 1)
		list = reverse_list(list, option);
	return (list);
}
