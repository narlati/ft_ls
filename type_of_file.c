/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type_of_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/02 10:48:19 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 15:02:56 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char			*print_type(struct s_arg *list)
{
	char		*result;

	if (!(result = ft_memalloc(sizeof(char *) * 2)))
		return (NULL);
	list->blockspecialfile == 1 ? result[0] = 'b' : '\0';
	list->characterspecialfile == 1 ? result[0] = 'c' : '\0';
	list->directory == 1 ? result[0] = 'd' : '\0';
	list->symboliclink == 1 ? result[0] = 'l' : '\0';
	list->socketlink == 1 ? result[0] = 's' : '\0';
	list->fifo == 1 ? result[0] = 'P' : '\0';
	list->regularfile == 1 ? result[0] = '-' : '\0';
	return (result);
}

void				ft_type_of_file2(struct s_arg **list)
{
	struct	stat	*info;

	if (!(info = ft_memalloc(sizeof(*info))))
		ft_putendl("error malloc ft_type_of_file2\n");
	if (!(lstat((*list)->name, info)))
		ft_putendl("error lstat ft_type_of_file2\n");
	(*list)->sizeoffile = (info->st_size);
}

void				ft_type_of_file(struct s_arg **list)
{
	struct stat		*info;
	struct passwd	*info2;
	struct group	*info3;

	if (!(info = ft_memalloc(sizeof(*info))))
		ft_putendl("error malloc ft_type_of_file\n");
	if (!(info = ft_memalloc(sizeof(*info2))))
		ft_putendl("error malloc ft_type_of_file\n");
	if (!(info = ft_memalloc(sizeof(*info3))))
		ft_putendl("error malloc ft_type_of_file\n");
	if (lstat((*list)->name, info) == -1)
		ft_putendl("error lstat ft_type_of_file\n");
	if (!(info2 = getpwuid(info->st_uid)))
		ft_putendl("error getpwuid\n");
	info3 = getgrgid(info->st_gid);
	(*list)->blockspecialfile = ((info->st_mode & S_IFMT) == S_IFBLK) ? 1 : 0;
	(*list)->characterspecialfile= ((info->st_mode & S_IFMT) == S_IFCHR) ? 1 : 0;
	(*list)->directory = ((info->st_mode & S_IFMT) == S_IFDIR) ? 1 : 0;
	(*list)->symboliclink = ((info->st_mode & S_IFMT) == S_IFLNK) ? 1 : 0;
	(*list)->socketlink = ((info->st_mode & S_IFMT) == S_IFSOCK) ? 1 : 0;
	(*list)->fifo = ((info->st_mode & S_IFMT) == S_IFIFO) ? 1 : 0;
	(*list)->regularfile = ((info->st_mode & S_IFMT) == S_IFREG) ? 1 : 0;
	(*list)->nblink = (info->st_nlink);
	(*list)->ownername = (info2->pw_name);
	(*list)->groupname = (info3->gr_name);
	ft_type_of_file2(list);
}
