/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse_list.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 13:46:42 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 12:04:07 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static struct s_arg	*reverse_dir(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp->isvalid == 0 && tmp->next)
		tmp = tmp->next;
	while (tmp->regularfile == 1 && tmp->next)
		tmp = tmp->next;
	while (tmp && tmp->directory == 1)
	{
		tmp2 = tmp->next;
		while (tmp2 && tmp2->directory == 1)
		{
			if (ft_strcmp(tmp->name, tmp2->name) < 0)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}

static struct s_arg	*reverse_reg(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp->isvalid == 0 && tmp->next)
		tmp = tmp->next;
	while (tmp && tmp->regularfile == 1)
	{
		tmp2 = tmp->next;
		while (tmp2 && tmp2->regularfile == 1)
		{
			if (ft_strcmp(tmp->name, tmp2->name) < 0)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}

static struct s_arg	*reverse_reg_t(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp->isvalid == 0 && tmp->next)
		tmp = tmp->next;
	while (tmp && tmp->regularfile == 1)
	{
		tmp2 = tmp->next;
		while (tmp2 && tmp2->regularfile == 1)
		{
			if (ft_strcmp(tmp->last_modified, tmp2->last_modified) > 0)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}

static struct s_arg	*reverse_dir_t(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp->isvalid == 0 && tmp->next)
		tmp = tmp->next;
	while (tmp->regularfile == 1 && tmp->next)
		tmp = tmp->next;
	while (tmp && tmp->directory == 1)
	{
		tmp2 = tmp->next;
		while (tmp2 && tmp2->directory == 1)
		{
			if (ft_strcmp(tmp->last_modified, tmp2->last_modified) > 0)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}

struct s_arg		*reverse_list(struct s_arg *list, struct s_options *option)
{
	if (option->t == 1)
	{
		list = reverse_reg_t(list);
		list = reverse_dir_t(list);
	}
	else
	{
		list = reverse_reg(list);
		list = reverse_dir(list);
	}
	return (list);
}
