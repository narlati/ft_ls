/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/17 11:19:36 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 14:54:30 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft/libft.h"
# include "libft/GNL/get_next_line.h"
# include <unistd.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <stdio.h>
# include <errno.h>
# include <time.h>
# include <pwd.h>
# include <grp.h>

struct		s_options
{
	int		l;
	int		upper_r;
	int		r;
	int		a;
	int		t;
};

struct		s_arg
{
	char	*name;
	char	*ownername;
	char	*groupname;
	size_t	sizeoffile;
	int		blockspecialfile;
	int		characterspecialfile;
	int		directory;
	int		symboliclink;
	int		socketlink;
	int		fifo;
	int		regularfile;
	int		isvalid;
	int		nblink;
	int		diralreadyuse;
	int		isprint;
	char	*last_modified;
	struct s_arg	*next;
};

/*error.c*/
void		ft_error(char *str);
void		ft_error_option(char error);

/*main.c*/
int			main(int argc, char **argv);
void		print(struct s_arg *arg);

/*sort_list.c*/
struct s_arg	*sort_list(struct s_arg *list, struct s_options *option);
struct s_arg	*sort_by_type(struct s_arg *list);

/*sort_by_type.c*/
void			list_cpy(struct s_arg *src, struct s_arg *cpy);
void			swap(struct s_arg *tmp, struct s_arg *tmp2);
struct s_arg	*sort_invalid(struct s_arg *list);
struct s_arg	*sort_reg(struct s_arg *list);

/*sort_ascii.c*/
struct s_arg	*sort_ascii_invalid(struct s_arg *list);
struct s_arg	*sort_ascii_reg(struct s_arg *list);
struct s_arg	*sort_ascii_dir(struct s_arg *list);
struct s_arg	*sort(struct s_arg *list);

/*sort_t.c*/
struct s_arg	*sort_t(struct s_arg *list);

/*reverse_list.c*/
struct s_arg	*reverse_list(struct s_arg *list, struct s_options *option);

/*make_list.c*/
struct s_options *get_option(char *name, int *i);
struct s_arg	*make_list(char *argv, struct s_arg *arg);

/*print_not_dir*/
struct s_arg	*print_list_not_dir(struct s_arg *list, struct s_options *option);
struct s_arg	*del_list_print(struct s_arg *list);

/*option_l.c*/
void			do_option_l_reg(struct s_arg *list);
void			do_option_l(struct s_arg *list);

/*user_group_other.c*/
char			*ft_user(struct s_arg *file);
char			*ft_group(struct s_arg *file);
char			*ft_other(struct s_arg *file);

/*type_of_file*/
void			ft_type_of_file(struct s_arg **file);
char			*print_type(struct s_arg *file);

#endif
