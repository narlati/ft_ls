/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/31 15:36:47 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 16:26:33 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

struct s_options		*get_option(char *name, int *i)
{
	int					j;
	struct s_options	*new;

	*new = (struct s_options){0, 0, 0, 0, 0};
	j = 1;
	if (name[0] == '-' && name[1])
	{
		while (name[j])
		{
			if (name[j] != 'l' && name[j] != 'r' && name[j] != 'R' &&
					name[j] != 'a' && name[j] != 't')
				ft_error_option(name[j]);
			new->l = (name[j] == 'l' ? 1 : new->l);
			new->r = (name[j] == 'r' ? 1 : new->r);
			new->upper_r = (name[j] == 'R' ? 1 : new->upper_r);
			new->a = (name[j] == 'a' ? 1 : new->a);
			new->t = (name[j] == 't' ? 1 : new->t);
			j++;
		}
		(*i)++;
	}
	return (new);
}

void					invalid_file(struct s_arg **arg)
{
	(*arg)->blockspecialfile = 0;
	(*arg)->characterspecialfile = 0;
	(*arg)->directory = 0;
	(*arg)->symboliclink = 0;
	(*arg)->socketlink = 0;
	(*arg)->regularfile = 0;
	(*arg)->last_modified = 0;
	(*arg)->nblink = 0;
	(*arg)->ownername = NULL;
	(*arg)->groupname = NULL;
	(*arg)->sizeoffile = 0;
}

struct s_arg			*init_arg(char *name)
{
	struct s_arg		*new;
	struct stat			*info;

	if (!(info = ft_memalloc(sizeof(*info))))
		return (NULL);
	if (!(new = ft_memalloc(sizeof(*new))))
		return (NULL);
	new->name = name;
	if (lstat(name, info) == -1)
		invalid_file(&new);
	else
	{
		new->isvalid = 1;
		ft_type_of_file(&new);
		new->last_modified = ft_itoa(info->st_ctime);
	}
	new->diralreadyuse = 0;
	new->isprint = 0;
	new->next = NULL;
	free(info);
	return (new);
}

struct s_arg			*add_to_list(char *name, struct s_arg *arg)
{
	struct s_arg		*new;
	struct stat			*info;

	if (!(info = ft_memalloc(sizeof(*info))))
		return (NULL);
	if (!(new = ft_memalloc(sizeof(*new))))
		return (NULL);
	new->name = name;
	if (lstat(name, info) == -1)
		invalid_file(&new);
	else
	{
		new->isvalid = 1;
		ft_type_of_file(&new);
		new->last_modified = ft_itoa(info->st_ctime);
	}
	new->diralreadyuse = 0;
	new->isprint = 0;
	new->next = arg;
	free(info);
	return (new);
}

struct s_arg			*make_list(char *argv, struct s_arg *arg)
{
	if (arg == NULL)
		arg = init_arg(argv);
	else
		arg = add_to_list(argv, arg);
	return (arg);
}
