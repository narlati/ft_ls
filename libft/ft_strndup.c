/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 14:06:39 by narlati           #+#    #+#             */
/*   Updated: 2016/12/13 16:52:06 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*ft_strndup(const char *s, size_t n)
{
	char	*dest;
	size_t	i;

	if (!s && ft_strlen(s) > n)
		return (NULL);
	if (!(dest = ft_strnew(n + 1)))
		return (NULL);
	i = 0;
	while ((s[i] != '\0') && (i < n))
	{
		dest[i] = s[i];
		i++;
	}
	return (dest);
}
