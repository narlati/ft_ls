/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 16:59:20 by narlati           #+#    #+#             */
/*   Updated: 2017/04/24 15:57:17 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

struct s_list		*ft_lstnew(void const *content, size_t content_size)
{
	struct s_list	*lst;
	void			*data;

	data = (void *)ft_memalloc(content_size + 1);
	if (!data)
		return (NULL);
	if (content == NULL)
	{
		data = NULL;
		content_size = 0;
	}
	else
		data = ft_memcpy(data, content, content_size);
	lst = ft_memalloc(sizeof(lst));
	if (lst != NULL)
	{
		lst->content_size = content_size;
		lst->next = NULL;
		lst->content = data;
	}
	return (lst);
}
