/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 08:33:45 by narlati           #+#    #+#             */
/*   Updated: 2016/12/15 10:13:46 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdlib.h>
#include <stdio.h>

int				endl(char **line, int *ret, char **str_buf)
{
	char		*str;
	char		*tmp;

	if (ft_strchr(*str_buf, '\n'))
	{
		tmp = ft_strndup(*str_buf, ft_strchr(*str_buf, '\n') - *str_buf);
		*line = ft_strdup(tmp);
		*ret = *ret - (ft_strchr(*str_buf, '\n') - *str_buf) - 1;
		str = ft_strdup(ft_strchr(*str_buf, '\n') + 1);
		ft_strdel(str_buf);
		*str_buf = str;
		ft_strdel(&tmp);
		return (1);
	}
	else
		*ret = *ret - ft_strlen(*str_buf);
	return (0);
}

int				read_buf(int *ret, char **str_buf, int fd)
{
	int			len;
	char		*str;
	char		*tmp;
	char		*buf;

	if (!(buf = ft_strnew(BUFF_SIZE + 1)))
		return (-1);
	len = read(fd, buf, BUFF_SIZE);
	if (len == -1)
		return (-1);
	str = ft_strdup(*str_buf);
	if (*str_buf)
		ft_strdel(str_buf);
	tmp = ft_strdup(buf);
	*str_buf = ft_strjoin(str, tmp);
	ft_strdel(&str);
	ft_strdel(&tmp);
	ft_strdel(&buf);
	if (*ret == -42)
		*ret = len;
	else if (*ret == 0 && len == 0)
		;
	else
		*ret = ft_strlen(*str_buf);
	return (1);
}

int				end_fd(int *ret, char **str_buf, char **line, int fd)
{
	char		*str;

	if (!(str = ft_strnew(*ret)))
		return (-1);
	if (*ret == 0 && **str_buf != '\0')
	{
		*line = ft_strdup(*str_buf);
		**str_buf = '\0';
		*ret = -3;
		return (1);
	}
	else if (*ret == 0 && **str_buf == '\0')
	{
		*ret = -3;
		return (0);
	}
	if (check_ret(ret, str_buf, fd, &str) == -1)
		return (-1);
	return (0);
}

int				check_ret(int *ret, char **str_buf, int fd, char **str)
{
	int			len;
	char		*buf;

	if (!(buf = ft_strnew(BUFF_SIZE + 1)))
		return (-1);
	ft_strncpy(*str, *str_buf, *ret);
	if (*ret == 1 && **str_buf == '\n')
	{
		if ((len = read(fd, buf, BUFF_SIZE)) == 0 && BUFF_SIZE != 1)
		{
			*ret = -3;
			**str_buf = '\0';
		}
		else if (len == -1)
			return (-1);
		else
		{
			ft_strdel(str_buf);
			*ret = *ret + len;
			*str_buf = ft_strjoin(*str, buf);
		}
	}
	ft_strdel(str);
	ft_strdel(&buf);
	return (1);
}

int				get_next_line(int const fd, char **line)
{
	static int	ret = -42;
	static char	*str_buf = NULL;

	if (fd < 0 || !line || BUFF_SIZE < 1)
		return (-1);
	*line = NULL;
	while (ret >= 0 || ret == -42)
	{
		if (!str_buf)
		{
			if (!(str_buf = ft_strnew(BUFF_SIZE + 1)))
				return (-1);
		}
		if (endl(line, &ret, &str_buf) == 1)
			return (1);
		else if (read_buf(&ret, &str_buf, fd) == -1)
			return (-1);
		if (end_fd(&ret, &str_buf, line, fd) == 1)
			return (1);
	}
	if (ret == -3)
		ret = -42;
	return (0);
}
