/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 16:45:47 by narlati           #+#    #+#             */
/*   Updated: 2016/12/15 13:29:45 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "../libft.h"
# include <unistd.h>

# define BUFF_SIZE 1

int	endl(char **line, int *ret, char **str_buf);
int	read_buf(int *ret, char **str_buf, int fd);
int	end_fd(int *ret, char **str_buf, char **line, int fd);
int	check_ret(int *ret, char **str_buf, int fd, char **str);
int	get_next_line(const int fd, char **line);

#endif
