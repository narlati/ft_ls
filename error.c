/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/24 14:14:46 by narlati           #+#    #+#             */
/*   Updated: 2017/06/01 16:22:56 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
	

void	ft_error(char *str)
{
	char *new;

	new = ft_strjoin("ft_ls : ", str);
	new = ft_strjoin(new, ": No such file or directory");
	ft_putendl(new);
	free(new);
}

void	ft_error_option(char error)
{
	ft_putstr_fd("ft_ls : illegal option -- ", 2);
	ft_putchar_fd(error, 2);
	ft_putendl_fd("\nusage : ft_ls [-lrRta] [file ...]", 2);
	exit(EXIT_FAILURE);
}
