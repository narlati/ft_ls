/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/17 11:24:20 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 14:29:26 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void					print(struct s_arg *arg)
{
	struct s_arg *tmp;

	tmp = arg;

	while (tmp != NULL)
	{
		if (tmp->isvalid == 0 || tmp->isvalid == 1)
		{
			ft_putendl(tmp->name);
			printf("blockspecialfile = %d\n", tmp->blockspecialfile);
			printf("characterspecialfile = %d\n", tmp->characterspecialfile);
			printf("directory = %d\n", tmp->directory);
			printf("symboliclink = %d\n", tmp->symboliclink);
			printf("socketlink = %d\n", tmp->socketlink);
			printf("fifo = %d\n", tmp->fifo);
			printf("regularfile = %d\n", tmp->regularfile);
			printf("nblink = %d\n", tmp->nblink);
			printf("is valid = %d\n", tmp->isvalid);
			printf("is diralreadyuse = %d\n",tmp->diralreadyuse);
			printf("is print = %d\n",tmp->isprint);
			printf("last_modified = %s\n\n", tmp->last_modified);
		}
		tmp = tmp->next;
	}
}

int						main(int argc, char **argv)
{
	struct s_options	*option = NULL;
	struct s_arg		*arg = NULL;
	int					i;

	i = 1;
	if (argc == 1)
	{
		ft_putendl("lol");
		return (1);
	}
	if (!(option = ft_memalloc(sizeof(*option))))
		return (1);
	option = get_option(argv[1], &i);
	while (i < argc)
	{
		arg = make_list(argv[i], arg);
		i++;
	}
	arg = sort_list(arg, option);
	arg = print_list_not_dir(arg, option);
	arg = del_list_print(arg);
/*	while (arg)
	{
		arg = get_list_dir(arg);
		arg = sort_list(arg);
		print_list_dir(arg);
		arg = del_list_print(arg);
		arg->next = arg;
	}
	del_list(arg);*/
	free(option);
	//print(arg);
	return (0);
}
