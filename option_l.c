/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   option_l.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 17:05:14 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 16:26:30 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char		*time(struct s_arg *file)
{
	char	*time;
	struct	*info;

	if (!(time = ft_memalloc(sizeof(char *) * 20)))
		return (NULL);
	if (!(info = ft_memalloc(sizeof(*info))))
		return (NULL);
	if (stat(file->name, info) == -1)
		return (NULL);

}

void	do_option_l_reg(struct s_arg *list)
{
	ft_putendl(list->name);
	ft_putstr(print_type(list));
	ft_putstr(ft_user(list));
	ft_putstr(ft_group(list));
	ft_putstr(ft_other(list));
	ft_putstr("	");
	ft_putnbr(list->nblink);
	ft_putstr("  ");
	ft_putstr(list->ownername);
	ft_putstr("  ");
	ft_putstr(list->groupname);
	ft_putstr("  ");
	ft_putnbr(list->sizeoffile);
	ft_putstr("  ");
	ft_putstr(time(list));
	ft_putstr("\n\n");

}

void	do_option_l(struct s_arg *list)
{
	if (list->directory == 0)
		do_option_l_reg(list);
	if (list->directory == 1)
		;
}
