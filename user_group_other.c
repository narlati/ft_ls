/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   user_group_other.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 18:14:10 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 15:02:39 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char			*ft_user(struct s_arg *file)
{
	char		*user;
	char		tmp;
	struct stat	*info;

	if (!(user = ft_memalloc(sizeof(char *) * 4)))
		return (NULL);
	if (!(info = ft_memalloc(sizeof(*info))))
		return (NULL);
	if (lstat(file->name, info) == -1)
		return (NULL);
	user[0] = (info->st_mode & S_IRUSR) ? 'r' : '-';
	user[1] = (info->st_mode & S_IWUSR) ? 'w' : '-';
	user[2] = (info->st_mode & S_IXUSR) ? 'x' : '-';
	tmp = user[2];
	user[2] = ((info->st_mode & S_ISUID) && tmp == 'x') ? 's' : 'x';
	user[2] = ((info->st_mode & S_ISUID) && tmp == '-') ? 'S' : '-';
	free(info);
	return (user);
}

char			*ft_group(struct s_arg *file)
{
	char		*group;
	char		tmp;
	struct stat *info;

	if (!(group = ft_memalloc(sizeof(char *) * 4)))
		return (NULL);
	if (!(info = ft_memalloc(sizeof(*info))))
		return (NULL);
	if (lstat(file->name, info) == -1)
		ft_putendl("error stat\n");
	group[0] = (info->st_mode & S_IRGRP) ? 'r' : '-';
	group[1] = (info->st_mode & S_IWGRP) ? 'w' : '-';
	group[2] = (info->st_mode & S_IXGRP) ? 'x' : '-';
	tmp = group[2];
	group[2] = ((info->st_mode & S_ISGID) && tmp == 'x') ? 's' : 'x';
	group[2] = ((info->st_mode & S_ISGID) && tmp == '-') ? 'S' : '-';
	free(info);
	return (group);
}

char			*ft_other(struct s_arg *file)
{
	char		*other;
	char		tmp;
	struct stat	*info;

	if (!(other = ft_memalloc(sizeof(char *) * 5)))
		return (NULL);
	if (!(info = ft_memalloc(sizeof(*info))))
		return (NULL);
	if (lstat(file->name, info) == -1)
		ft_putendl("error stat\n");
	other[0] = (info->st_mode & S_IROTH) ? 'r' : '-';
	other[1] = (info->st_mode & S_IWOTH) ? 'w' : '-';
	other[2] = (info->st_mode & S_IXOTH) ? 'x' : '-';
	tmp = other[2];
	other[2] = ((info->st_mode & S_ISVTX) && tmp == 'x') ? 't' : 'x';
	other[2] = ((info->st_mode & S_ISVTX) && tmp == '-') ? 'T' : '-';
	free(info);
	return (other);
}
