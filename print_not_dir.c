/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_not_dir.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 14:38:18 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 13:05:23 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

struct s_arg		*del_list_print(struct s_arg *list)
{
	struct s_arg	*tmp;

	while (list)
	{
		if (list->isprint == 1)
		{
			tmp = list->next;
			free(list);
			list = tmp;
		}
		else
			list = list->next;
	}
	return (tmp);
}

struct s_arg	*print_list_not_dir(struct s_arg *list, struct s_options *option)
{
	struct s_arg *tmp;

	tmp = list;
	while (tmp && tmp->directory == 0)
	{
		if (tmp->isvalid == 0)
			ft_error(tmp->name);
		else
		{	
			if (option->l == 1)
				do_option_l(tmp);
			else
				ft_putendl(tmp->name);
		}
		tmp->isprint = 1;
		tmp = tmp->next;
	}
	return (list);
}
