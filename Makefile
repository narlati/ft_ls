# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: narlati <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/13 10:52:15 by narlati           #+#    #+#              #
#    Updated: 2017/06/02 16:28:49 by narlati          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

DEP= Makefile ft_ls.h libft/libft.h
src = type_of_file.c main.c error.c make_list.c sort_list.c sort_by_type.c sort_ascii.c sort_t.c reverse_list.c print_not_dir.c option_l.c user_group_other.c
NAME=ft_ls
FLAG= #-Wall -Wextra -Werror#

obj=$(src:.c=.o)

all: $(NAME)

$(NAME): libft/libft.a $(obj) $(DEP)
	gcc $(FLAG) -I libft -I. -o $(NAME) $(obj) libft/libft.a

%.o: %.c $(DEP)
	gcc $(FLAG) -I libft -I. -o $@ -c $<

clean:
	rm -f $(obj)
	make clean -C libft/

fclean: clean
	rm -f $(NAME)
	make fclean -C libft/

re: fclean
	make $(NAME)

libft/libft.a: libft/
	make -C ./libft/
