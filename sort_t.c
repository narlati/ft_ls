/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_ascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 10:49:00 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 12:02:08 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static struct s_arg	*sort_t_invalid(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp && tmp->isvalid == 0)
	{
		tmp2 = tmp->next;
		while (tmp2 && tmp2->isvalid == 0)
		{
			if (ft_strcmp(tmp->name, tmp2->name) > 0)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}

static struct s_arg	*sort_t_reg(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp->isvalid == 0 && tmp->next)
		tmp = tmp->next;
	while (tmp && tmp->regularfile == 1)
	{
		tmp2 = tmp->next;
		while (tmp2 && tmp2->regularfile == 1)
		{
			if (ft_strcmp(tmp->last_modified, tmp2->last_modified) < 0)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}

static struct s_arg	*sort_t_dir(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp->isvalid == 0 && tmp->next)
		tmp = tmp->next;
	while (tmp->regularfile == 1 && tmp->next)
		tmp = tmp->next;
	while (tmp && tmp->directory == 1)
	{
		tmp2 = tmp->next;
		while (tmp2 && tmp2->directory == 1)
		{
			if (ft_strcmp(tmp->last_modified, tmp2->last_modified) < 0)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}

struct s_arg		*sort_t(struct s_arg *list)
{
	list = sort_t_invalid(list);
	list = sort_t_reg(list);
	list = sort_t_dir(list);
	return (list);
}
