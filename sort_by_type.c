/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_by_type.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: narlati <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/01 10:32:58 by narlati           #+#    #+#             */
/*   Updated: 2017/06/02 13:01:39 by narlati          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void				list_cpy(struct s_arg *src, struct s_arg *cpy)
{
	src->name = cpy->name;
	src->isvalid = cpy->isvalid;
	src->blockspecialfile = cpy->blockspecialfile;
	src->characterspecialfile = cpy->characterspecialfile;
	src->directory = cpy->directory;
	src->symboliclink = cpy->symboliclink;
	src->socketlink = cpy->socketlink;
	src->fifo = cpy->fifo;
	src->regularfile = cpy->regularfile;
	src->diralreadyuse = cpy->diralreadyuse;
	src->last_modified = cpy->last_modified;
}

void				swap(struct s_arg *tmp, struct s_arg *tmp2)
{
	struct s_arg	tmp3;

	tmp3 = *tmp;
	list_cpy(tmp, tmp2);
	list_cpy(tmp2, &tmp3);
}

struct s_arg		*sort_invalid(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp)
	{
		tmp2 = tmp->next;
		while (tmp2)
		{
			if (tmp->isvalid > tmp2->isvalid)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}

struct s_arg		*sort_reg(struct s_arg *list)
{
	struct s_arg	*tmp;
	struct s_arg	*tmp2;

	tmp = list;
	while (tmp->isvalid == 0 && tmp->next)
		tmp = tmp->next;
	while (tmp)
	{
		tmp2 = tmp->next;
		while (tmp2)
		{
			if (tmp->directory > tmp2->directory)
				swap(tmp, tmp2);
			tmp2 = tmp2->next;
		}
		tmp = tmp->next;
	}
	return (list);
}
